requests==2.25.1
backoff==1.11.1
python-dotenv==0.19.0
websocket_client==1.2.1
